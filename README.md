nfs_link
=========



Requirements
------------
This role needs privilege escalation. You have
to set `become: yes` in your playbook and be sure
your **remote_user** is allowed.

Role Variables
--------------

You can find all variables in `vars/main.yml` and `defaults/main.yml`

You can overwrite them inside your playbook.

3 variables are **mandatory**:
- OU_path: where you want to add your computer
- sys_user: your privileged user
- sys_password: privileged password

Example Playbook
----------------

    ---
    - name: "nfs_ucbl1 example playbook"
      hosts: all
      become: yes
      roles:
        - nfs_ucbl1
      vars:
        OU_path: "OU=computers,OU=univ-lyon1"
        sys_user: "sys_user"
      vars_prompt:
        - name: "sys_password"
          prompt: "Enter sys_user's password "
          private: yes


License
-------
Depoy NFS and integrate your computer in AD
Copyright © 2022 Romain CHANU

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Author Information
------------------
Romain CHANU  
UCBL1
