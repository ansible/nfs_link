#!/bin/bash
# Script autohome permettant de faire un lien
# symbolique vers HOMELYON1 dans le home de l'utilisateur
klist -l > /dev/null
KRETURN=$?
if [ $KRETURN -eq 0 ]
then
    if [ ! -h $HOME/HOMELYON1 ]
    then
        if [ $(id -g) -eq 2000 ]
        then
            DIR="pers"
        else
            DIR="etu"
        fi
        ln -s {{mnt_nfs_path}}/$DIR/$USER $HOME/HOMELYON1
    fi
fi
